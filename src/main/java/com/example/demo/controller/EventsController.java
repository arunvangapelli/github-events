package com.example.demo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.service.Event;
import com.example.demo.service.Events;
import com.example.demo.service.EventsService;

@Controller
public class EventsController {

    @Autowired
    private EventsService eventsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(EventsController.class);

    @GetMapping("/events")
    public String greetingForm(Model model) {
        model.addAttribute("events", new Events());
        return "events";
    }

    @PostMapping(value = "/events")
    public String submitEvents(@ModelAttribute Events events, Model model) {
        LOGGER.info("Owner = {}, Repo = {}, Event type = {}", events.getOwner(), events.getRepo(), events.getType());
        List<Event> list = eventsService.getEvents(events);

        // To display results.
        model.addAttribute("list", list);
        return "result";
    }

}
