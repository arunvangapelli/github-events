package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class EventsService {

    @Autowired
    private RestTemplate restTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(EventsService.class);
    private String url = "https://api.github.com/repos/{owner}/{repo}/events";

    public List<Event> getEvents(Events events) {
        ResponseEntity<String> res = null;
        List<Event> filteredList = null;
        try {
            res = restTemplate.getForEntity(url, String.class, events.getOwner(), events.getRepo());

            ObjectMapper mapper = new ObjectMapper();
            List<Event> eventJsonList = mapper.readValue(res.getBody(), new TypeReference<List<Event>>() {
            });

            filteredList = eventJsonList.stream().filter(event -> event.getType().contains(events.getType()))
                    .collect(Collectors.toList());

        } catch (Exception ex) {
            LOGGER.error("Exception ocurred, message = {} ", ex.getMessage(), ex);
        }
        return filteredList;
    }

}
