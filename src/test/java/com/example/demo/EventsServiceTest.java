package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.demo.service.Event;
import com.example.demo.service.Events;
import com.example.demo.service.EventsService;

@RunWith(MockitoJUnitRunner.class)
public class EventsServiceTest {

    @InjectMocks
    private EventsService eventsServiceMock;
    @Mock
    private RestTemplate restTemplate;
    String json;
    ResponseEntity<Object> res;

    @Before
    public void beforeTest() {
        json = "[{\n" + "                \"id\": \"9636385793\",\n" + "                \"type\": \"ForkEvent\",\n"
                + "                \"actor\": {\n" + "                        \"id\": 40349541,\n"
                + "                        \"login\": \"AnimMouse\"\n" + "                },\n"
                + "                \"created_at\": \"2019-05-16T05:45:01Z\"\n" + "        },\n" + "        {\n"
                + "                \"id\": \"9636385794\",\n" + "                \"type\": \"WatchEvent\",\n"
                + "                \"actor\": {\n" + "                        \"id\": 40349542,\n"
                + "                        \"login\": \"AnimMouse\"\n" + "                },\n"
                + "                \"created_at\": \"2019-05-16T05:45:01Z\"\n" + "        }\n" + "]";

        res = new ResponseEntity<>(json, HttpStatus.OK);
        // Set Events API response.
        Mockito.when(restTemplate.getForEntity(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any()))
                .thenReturn(res);
    }

    @Test
    public void forkEventTypeTest() {

        Events events = new Events();
        events.setType("ForkEvent");

        List<Event> list = eventsServiceMock.getEvents(events);

        assertEquals(1, list.size());
        assertEquals("9636385793", list.get(0).getId());
        assertEquals("ForkEvent", list.get(0).getType());
    }

    @Test
    public void watchEventTypeTest() {

        Events events = new Events();
        events.setType("WatchEvent");

        List<Event> list = eventsServiceMock.getEvents(events);

        assertEquals(1, list.size());
        assertEquals("9636385794", list.get(0).getId());
        assertEquals("WatchEvent", list.get(0).getType());
    }

}
